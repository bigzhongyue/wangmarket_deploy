package com.xnx3.deploy;

import java.util.ArrayList;
import java.util.List;

import com.xnx3.FileUtil;

/**
 * jar文件的比对，用于版本升级时，判断两个jar文件的不同，有哪些是新增、或删除的
 * @author 管雷鸣
 */
public class JarDiff {
	
	public static void main(String[] args) {
		String path = JarDiff.class.getResource("./").getPath();
		System.out.println(path);
		
		//取旧版本的jar包文件名的数组
		String oldString = FileUtil.read(path+"old.version");
//		System.out.println(oldString);
		String[] olds = oldString.trim().split("\n|\r");
		System.out.println("旧版本jar包数量："+olds.length);
		
		//取信版本的jar包文件名的数组
		String newString = FileUtil.read(path+"new.version");
//		System.out.println(newString);
		String[] news = newString.trim().split("\n|\r");
		System.out.println("新版本jar包数量："+news.length);
		
		
		List<String> addList = diff(olds, news);
		System.out.println("新版本减少的jar包："+addList.size()+"个");
		for (int i = 0; i < addList.size(); i++) {
			System.out.println(addList.get(i));
		}
		
		System.out.println("-----------------");
		
		List<String> subtractList = diff(news, olds);
		System.out.println("新版本增加的jar包："+subtractList.size()+"个");
		for (int i = 0; i < subtractList.size(); i++) {
			System.out.println(subtractList.get(i));
		}
		
		
	}
	
	/**
	 * 按行比较，比较 array1 比 array2 多了哪些
	 * @param array1
	 * @param array2
	 * @return 多的以list返回
	 */
	public static List<String> diff(String[] array1, String[] array2) {
		List<String> list = new ArrayList<String>();
		
		for (int i = 0; i<array1.length; i++) {
			String version = array1[i].trim();
			if(version.length() > 0) {
				
				//判断旧版本中是否存在
				boolean find = false; //要是发现一样的，那就是true
				for (int j = 0; j<array2.length; j++) {
					String item = array2[j].trim();
					if(item.length() > 0) {
						if(item.equalsIgnoreCase(version)) {
							find = true;	//发现一样的了，
							continue;
						}
					}
				}
				if(!find) {
					list.add(version);
				}
			}
		}
		
		return list;
	}
	
}
